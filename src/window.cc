/* Copyright (c) 2020 Adélie Software in the Public Benefit, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *    3. Neither the name of the copyright holder nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "window.hh"

Window::Window(QWidget *parent) : QWidget(parent) {
    layMain = new QHBoxLayout();
    laySide = new QVBoxLayout();
    layButtons = new QHBoxLayout();

    viewData = new QListView();
    laySide->addWidget(viewData);

    btnAdd    = new QToolButton();
    btnRemove = new QToolButton();
    btnConfig = new QToolButton();
    btnReload = new QToolButton();

    QAction *actAdd    = new QAction(btnAdd);
    QAction *actRemove = new QAction(btnRemove);
    QAction *actConfig = new QAction(btnConfig);
    QAction *actReload = new QAction(btnReload);

    actAdd->setIcon(QIcon(":icons/add.svg"));
    actRemove->setIcon(QIcon(":icons/remove.svg"));
    actConfig->setIcon(QIcon(":icons/config.svg"));
    actReload->setIcon(QIcon("icons/reload.svg"));

    btnAdd->setDefaultAction(actAdd);
    btnRemove->setDefaultAction(actRemove);
    btnConfig->setDefaultAction(actConfig);
    btnReload->setDefaultAction(actReload);

    layButtons->addWidget(btnAdd);
    layButtons->addWidget(btnRemove);
    layButtons->addWidget(btnConfig);
    layButtons->addWidget(btnReload);
    laySide->addLayout(layButtons);

    viewPage = new QTextEdit();
    viewPage->setReadOnly(true);
    layMain->addLayout(laySide);
    layMain->addWidget(viewPage);
}
Window::~Window() {}

void Window::display() {
    show();
}

