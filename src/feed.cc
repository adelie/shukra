/* Copyright (c) 2020 Adélie Software in the Public Benefit, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *    3. Neither the name of the copyright holder nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "shukra.hh"
#include "feed.hh"

Feed::Feed(QListWidget *parent) : QListWidgetItem(parent, LIST_FEED) {}

Feed *Feed::feedFromData(QString name, QUrl source, Frequency frequency) {
    Feed *ret = new Feed();

    ret->setName(name);
    ret->setSource(source);
    ret->setUpdateFrequency(frequency);

    return ret;
}
Feed *Feed::feedFromData(QString name, QString source, Frequency frequency) {
    Feed *ret = new Feed();

    ret->setName(name);
    ret->setSource(source);
    ret->setUpdateFrequency(frequency);

    return ret;
}
void Feed::setName(const QString &name) {
    _name = name;
}
void Feed::setSource(const QUrl &source) {
    _source = source;
}
void Feed::setSource(const QString &source) {
    _source = QUrl(source);
}
void Feed::setUpdateFrequency(const Frequency &updateFrequency) {
    _updateFrequency = updateFrequency;
}

