/* Copyright (c) 2020 Adélie Software in the Public Benefit, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *    3. Neither the name of the copyright holder nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>

#include "shukra.hh"
#include "feedconfig.hh"

Shukra::Shukra(int argc, char **argv) : QApplication(argc, argv) {
    window = new Window();
    net = new QNetworkAccessManager();
    cfg = new QSettings("Adélie Linux Team", applicationName());
    db = QSqlDatabase::addDatabase(
        "QSQLITE",
        getConfigPath("data.db")
    );
    db.open();/*
    if (cfg->value("firstrun", true).toBool() == true) {
        setup();
        cfg->setValue("firstrun", false);
        cfg->setValue("font/size", 16);
        cfg->setValue("font/face", "DejaVu Sans");
    }*/
}

int Shukra::start() const {
    window->display();
    return exec();
}

QString Shukra::getConfigPath(const QString &filename) {
    QString path = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation).at(0);
    return path + "/" + filename;
}

void Shukra::setup() {
    QStringList queries = {
        "CREATE TABLE IF NOT EXISTS agency (id INTEGER PRIMARY KEY, url VARCHAR(256) NOT NULL, name VARCHAR(128), type VARCHAR(32) NOT NULL, auth_required BOOL DEFAULT false, auth_type VARCHAR(32));",
        "CREATE TABLE IF NOT EXISTS article (id INTEGER PRIMARY KEY, url VARCHAR(256) NOT NULL, title VARCHAR(256), subtitle VARCHAR(256), author VARCHAR(64), updated DATETIME, published DATETIME, content TEXT);",
        "CREATE TABLE IF NOT EXISTS tag (id INTEGER PRIMARY KEY, name VARCHAR(64) NOT NULL);",
        "CREATE TABLE IF NOT EXISTS bucket (id INTEGER PRIMARY KEY, name VARCHAR(64) NOT NULL);",
        "CREATE TABLE IF NOT EXISTS bucket_tag (bucket_id INTEGER REFERENCES bucket (id), tag_id INTEGER REFERENCES tag (id));",
        "CREATE TABLE IF NOT EXISTS bucket_article (bucket_id INTEGER REFERENCES bucket (id), article_id INTEGER REFERENCES article (id));",
        "CREATE TABLE IF NOT EXISTS agency_auth (agency_id INTEGER PRIMARY KEY, cookie TEXT) FOREIGN KEY (agency_id) REFERENCES agency (id);"
    };
    for (int i = 0; i < queries.size(); i++) {
        QSqlQuery query = db.exec(queries.at(i));
        QSqlError error = query.lastError();
        if (error.type() != 0) {
            std::cerr << "Failed to execute query <<" << queries.at(i).toStdString() << ">>: " << error.text().toStdString() << std::endl;
        }
    }
}

void Shukra::addFeed() {
    FeedConfig *cfg = new FeedConfig();
    Feed *feed = new Feed();
    /* connections */
}

