/* Copyright (c) 2020 Adélie Software in the Public Benefit, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *    3. Neither the name of the copyright holder nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "feedconfig.hh"

FeedConfig::FeedConfig(QWidget *parent) : QWidget(parent) {
    layMain = new QFormLayout();
    editName = new QLineEdit();
    editURL = new QLineEdit();
    pickType = new QComboBox();
    pickFrequency = new QComboBox();

    /*
       TODO - make this modular; for now, we only support sources which
              include entire articles in Atom
    */
    pickType->addItem("Atom");
    pickType->setCurrentIndex(0);
    pickType->setDisabled(true);

    pickFrequency->addItem("5 minutes", QVariant(Feed::Frequency::Minutes_5));
    pickFrequency->addItem("10 minutes", QVariant(Feed::Frequency::Minutes_10));
    pickFrequency->addItem("15 minutes", QVariant(Feed::Frequency::Minutes_15));
    pickFrequency->addItem("30 minutes", QVariant(Feed::Frequency::Minutes_30));
    pickFrequency->addItem("1 hour", QVariant(Feed::Frequency::Hours_1));
    pickFrequency->addItem("3 hours", QVariant(Feed::Frequency::Hours_3));
    pickFrequency->addItem("6 hours", QVariant(Feed::Frequency::Hours_6));
    pickFrequency->addItem("12 hours", QVariant(Feed::Frequency::Hours_12));
    pickFrequency->addItem("24 hours", QVariant(Feed::Frequency::Day));
    /* default to 15 minutes */
    pickFrequency->setCurrentIndex(2);

    layMain->addRow("Name:", editName);
    layMain->addRow("URL:", editURL);
    layMain->addRow("Type:", pickType);
    layMain->addRow("Update frequency:", pickFrequency);

    setLayout(layMain);
}

QString FeedConfig::name() const {
    return editName->text();
}
void FeedConfig::setName(QString name) {
    editName->setText(name);
}

QUrl FeedConfig::source() const {
    return QUrl(editURL->text());
}
void FeedConfig::setSource(QString source) {
    editURL->setText(source);
}
void FeedConfig::setSource(QUrl source) {
    editURL->setText(source.toString());
}

Feed::Frequency FeedConfig::updateFrequency() const {
    return pickFrequency->currentData().value<Feed::Frequency>();
}
void FeedConfig::setUpdateFrequency(Feed::Frequency frequency) {
    int index = pickFrequency->findData(QVariant(frequency));
    if (index < 0) {
        /* FIXME - maybe Vader someday later */
        return;
    }
    pickFrequency->setCurrentIndex(index);
}

