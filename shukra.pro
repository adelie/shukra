QT += core gui widgets sql network

HEADERS += \
    include/shukra.hh \
    include/feed.hh \
    include/window.hh \
    include/feedconfig.hh \
    include/article.hh
SOURCES += \
    src/feedconfig.cc \
    src/feed.cc \
    src/shukra.cc \
    src/window.cc \
    src/article.cc \
    src/main.cc

RESOURCES += icons.qrc

TEMPLATE = app
TARGET   = Shukra
VERSION  = 0.1.0
QMAKE_CFLAGS = -Iinclude
QMAKE_CXXFLAGS = -Iinclude

