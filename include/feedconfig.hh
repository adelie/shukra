
#pragma once

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>

#include "feed.hh"

class FeedConfig : QWidget {
public:
    FeedConfig(QWidget *parent = nullptr);

    QString name() const;
    void setName(QString name);
    void setSource(QString source);
    QUrl source() const;
    void setSource(QUrl source);
    Feed::Frequency updateFrequency() const;
    void setUpdateFrequency(Feed::Frequency frequency);

private:
    QFormLayout *layMain;
    QLineEdit *editName; // temporary user label?
    QLineEdit *editURL;
    QComboBox *pickType;
    QComboBox *pickFrequency;
    /* authentication stuff here */
};

