#pragma once

#include <QApplication>
#include <QList>
#include <QNetworkAccessManager>
#include <QSettings>
#include <QStandardPaths>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include "feed.hh"
#include "window.hh"

enum {
    LIST_FEED = 1001,
    LIST_BUCKET,
};

class Shukra : QApplication {
    Q_OBJECT

public:
    Shukra(int argc, char **argv);

    QNetworkAccessManager *net;
    QSettings *cfg;

    int start() const;

    void addFeed();

private:
    Window *window;
    QList<Feed> *feeds;
    QSqlDatabase db;

    void setup();
    QString getConfigPath(const QString &filename);
};

