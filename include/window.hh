#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListView>
#include <QLabel>
#include <QTextEdit>
#include <QToolButton>
#include <QAction>

class Window : QWidget {
public:
    Window(QWidget *parent = nullptr);
    ~Window();

    void display();

private:
    QHBoxLayout *layMain;
    QVBoxLayout *laySide;
    QHBoxLayout *layButtons;
    QListView *viewData;
    QTextEdit *viewPage;
    QToolButton *btnAdd;
    QToolButton *btnRemove;
    QToolButton *btnConfig;
    QToolButton *btnReload;
};

