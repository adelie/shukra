
#pragma once

#include <QListWidgetItem>
#include <QListWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QUrl>

class Feed : public QListWidgetItem {
    Q_OBJECT

    Q_PROPERTY(QString name MEMBER _name WRITE setName NOTIFY nameChanged);
    Q_PROPERTY(QUrl source MEMBER _source WRITE setSource NOTIFY sourceChanged);
    Q_PROPERTY(Frequency updateFrequency MEMBER _updateFrequency WRITE setUpdateFrequency NOTIFY updateFrequencyChanged);

public:
    Feed(QListWidget *parent = nullptr);

    enum Frequency {
        Minutes_5  =    5,
        Minutes_10 =   10,
        Minutes_15 =   15,
        Minutes_30 =   30,
        Hours_1    =   60,
        Hours_2    =  120,
        Hours_3    =  180,
        Hours_6    =  360,
        Hours_12   =  720,
        Day        = 1440,
    };
    Q_ENUM(Frequency);

    static Feed *feedFromData(QString name, QUrl source, Frequency updateFrequency);
    static Feed *feedFromData(QString name, QString source, Frequency updateFrequency);

    void setName(const QString &name);
    void setSource(const QUrl &source);
    void setSource(const QString &source);
    void setUpdateFrequency(const Frequency &updateFrequency);

signals:
    void nameChanged(const QString &newName);
    void sourceChanged(const QString &newSource);
    void updateFrequencyChanged(const Feed::Frequency newFrequency);

private:
    QString _name;
    QUrl _source;
    Feed::Frequency _updateFrequency;
};
