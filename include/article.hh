#pragma once

#include <QUrl>
#include <QString>
#include <QDateTime>

class Article {
public:
    Article();
    ~Article();

private:
    QUrl source;
    QString title;
    QString subtitle = nullptr;
    QString author = nullptr;
    QDateTime published;
    QString content;
};

