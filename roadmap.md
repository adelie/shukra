This document describes the goals and milestones for Shukra releases.

 0.1
-----
* Atom support

 0.2
-----
* Tagging of agencies
* Buckets for collecting multiple tags under a single label

 0.3
-----
* Ability to scan articles for tags in order to auto-tag
* Regular expressions for tags?

 0.4
-----
* TBD

 1.0
-----
* Modular interface for adding article-fetching and authentication
* Module for authenticating Scroll
* Modules for accessing Scroll-supported sites, e.g. The Verge

