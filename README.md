Shukra
------

**Shukra** is a news aggregator, designed to be an accessible, Libre equivalent of Apple News. It features integration for [Scroll](https://scroll.com) and other content support networks in order to provide an ad-free experience without compromising news providers' ability to pay their journalists a fair wage.

Building
--------

To build Shukra, you will need Qt 5.

```
$ qmake shukra.pro
$ make
```

Copyright
---------

Shukra is made available under the terms of the `BSD-3-Clause` license described in `LICENSE.md`.
